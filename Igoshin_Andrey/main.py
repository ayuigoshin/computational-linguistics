#!/usr/bin/env python
# -*- coding: utf8 -*-
from lxml import etree
import re
import os
from Igoshin_Andrey.com.linguistics.homework2.IndexBuilder import IndexBuilder, IndexUtils, TfidfIndexBuilder
from Igoshin_Andrey.com.linguistics.homework2.util import each

# Homework 1
pattern = 'articles/{0:s}_{1:d}.txt'
topics = ['design', 'marketing']

html_directory = 'html'
output_directory = 'output'

output_dir_path = os.path.join(os.path.curdir, output_directory)
html_dir_path = os.path.join(output_dir_path, html_directory)

output_file_path_pattern = os.path.join(html_dir_path, '{0:s}.html')

if not os.path.exists(output_dir_path):
    os.mkdir(output_dir_path)
if not os.path.exists(html_dir_path):
    os.mkdir(html_dir_path)

for topic in topics:
    articles = [open(pattern.format(topic, i), 'r').read() for i in range(1, 11)]

    page = etree.Element('html')

    head = etree.SubElement(page, 'head')
    body = etree.SubElement(page, 'body')

    for article in articles:
        p = etree.SubElement(body, 'p')
        splitted = re.split(r"\n", article)
        filtered = filter(lambda string: string.strip(), splitted)
        formatted = [re.sub(r"[\.,\-:;\n!?]", '', p) for p in filtered]
        for string in formatted:
            div = etree.SubElement(p, 'div')
            div.text = string

    with open(output_file_path_pattern.format(topic), 'w') as output:
        output.write(etree.tounicode(page, pretty_print=True))


# Homework 2
index = IndexBuilder(output_file_path_pattern.format(topics[0])).build()
query = input("\nInsert search query:")
found = index.search(query)
if found:
    for sentence in found:
        print(sentence)


# Homework 3
tfidf = TfidfIndexBuilder(output_file_path_pattern.format(topics[0])).build()
top5 = tfidf.get_most_relevant_docs(query, 5)
each(top5, print)


