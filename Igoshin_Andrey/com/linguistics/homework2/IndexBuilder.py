import json
import os

from collections import Counter
import math

import pymorphy2
from nltk.corpus import stopwords
from lxml import etree
import numpy as np
from scipy import spatial
import operator

from Igoshin_Andrey.com.linguistics.homework2.util import each


class IndexBuilder:
    _sentences = []
    _filename = "output/index.json"
    
    def __init__(self, filePath):
        super().__init__()
        self.html_to_list(filePath)
    
    def html_to_list(self, filePath):
        self._sentences = []
        tree = etree.parse(filePath)
        doc = tree.getroot()
        for node in doc:
            for p in node:
                for div in p:
                    self._sentences.append(div.text)
    
    def build(self):
        print("-----BUILD START------")
        if os.path.exists(self._filename):
            with open(self._filename, 'r') as fp:
                index = json.load(fp)
        else:
            # File does not exist, create an index and save it to file
            index = self._get_reversed_index()
            with open(self._filename, 'w') as fp:
                json.dump(index, fp, sort_keys=True, indent=4)
        
        print("-----BUILD FINISHED------")
        return Index(index, self._sentences)
    
    def _get_reversed_index(self):
        index = {}
        stop_words = stopwords.words('russian')
        i = 0
        for sent in self._sentences:
            splitted = sent.split(" ")
            splitted = [word for word in splitted if (word not in stop_words and word != " " and word != "")]
            splitted = IndexUtils.normalize(splitted)
            self._update_reversed_index(index, splitted, i)
            i += 1
        return index
    
    @staticmethod
    def _update_reversed_index(index, words, i):
        for word in words:
            if index.get(word):
                index.get(word).append(i)
            else:
                index.update({word: [i]})

class TfidfIndexBuilder(IndexBuilder):
    _sentences = []
    _filename = "output/tfidf.json"

    def __init__(self, filePath):
        super().__init__(filePath)

    def build(self):
        print("-----BUILD START------")
        if os.path.exists(self._filename):
            with open(self._filename, 'r') as fp:
                index = json.load(fp)
        else:
            # File does not exist, create an index and save it to file
            index = self._get_reversed_index()
            with open(self._filename, 'w') as fp:
                json.dump(index, fp, sort_keys=True, indent=4)

        print("-----BUILD FINISHED------")
        return TfidfIndex(index, self._sentences)

    def _get_reversed_index(self):
        index = {}
        stop_words = stopwords.words('russian')
        normalized = list(
            map(
                lambda sent: IndexUtils.normalize(
                    [word for word in sent.split(" ") if (word not in stop_words and word != " " and word != "")]
                ),
                self._sentences
            )
        )
        tfidf_sents = self._compute_tfidf(normalized)

        for i, sentence in enumerate(tfidf_sents):
            self._update_reversed_index(index, sentence, i)
        return index

    @staticmethod
    def _update_reversed_index(index, tfidf_sents, i):
        for word, tfidf in tfidf_sents.items():
            if index.get(word):
                index.get(word).update({i: tfidf})
            else:
                index.update({word: {i: tfidf}})

    @staticmethod
    def _compute_tfidf(sentences):
        def compute_tf(text):
            tf_text = Counter(text)
            for i in tf_text:
                tf_text[i] /= float(len(tf_text))
            return tf_text

        # 2nd param for purity
        def compute_idf(word, text):
            return math.log10(len(text) / sum([1.0 for i in text if word in i]))

        def map_to_tfidf_dict(text):
            tf_idf_dictionary = {}
            computed_tf = compute_tf(text)
            for word in computed_tf:
                tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, sentences)
            return tf_idf_dictionary

        return list(map(map_to_tfidf_dict, sentences))


class Index:
    _index = None
    _sentences = None
    
    def __init__(self, index, sentences):
        super().__init__()
        self._index = index
        self._sentences = sentences
    
    def search(self, query):
        words = IndexUtils.normalize(query.split(" "))
        results = {}
        for word in words:
            found = self._index.get(word)
            if found:
                if not results:
                    results = set(found)
                else:
                    results = results.intersection(found)
        return [self._sentences[i] for i in results]


class TfidfIndex(Index):

    def get_most_relevant_docs(self, query, count=5):
        docs = set()
        query = IndexUtils.normalize(query.split(" "))

        for word in query:
            if self._index.get(word):
                for sent_ind in list(self._index.get(word).keys()):  # put all sentences from index into set
                    docs.add(sent_ind)

        docs_tfidf_arrs = {}
        for doc in docs:
            doc_vector = []
            for word in query:
                if self._index.get(word) and self._index.get(word).get(doc):  # td-ifd value is stored in index
                    doc_vector.append(self._index.get(word).get(doc))
                else:
                    doc_vector.append(0)
            docs_tfidf_arrs.update({doc: doc_vector})

        tfidf_vector = np.ones(len(query))
        cosine_distances = {}
        for doc_id, doc_vector in docs_tfidf_arrs.items():
            cosine_dist = spatial.distance.cosine(doc_vector, tfidf_vector)
            cosine_distances.update({doc_id: cosine_dist})

        return [self._sentences[int(doc[0])] for doc in
                sorted(cosine_distances.items(), key=operator.itemgetter(1))[:count]]


class IndexUtils:

    @staticmethod
    def normalize(words):
        morph = pymorphy2.MorphAnalyzer()
        normalized = list(
            map(lambda word: morph.parse(word)[0].normal_form, words)
        )
        return normalized



